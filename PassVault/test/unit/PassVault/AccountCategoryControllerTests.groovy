package PassVault



import org.junit.*
import grails.test.mixin.*

@TestFor(AccountCategoryController)
@Mock(AccountCategory)
class AccountCategoryControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/accountCategory/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.accountCategoryInstanceList.size() == 0
        assert model.accountCategoryInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.accountCategoryInstance != null
    }

    void testSave() {
        controller.save()

        assert model.accountCategoryInstance != null
        assert view == '/accountCategory/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/accountCategory/show/1'
        assert controller.flash.message != null
        assert AccountCategory.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/accountCategory/list'

        populateValidParams(params)
        def accountCategory = new AccountCategory(params)

        assert accountCategory.save() != null

        params.id = accountCategory.id

        def model = controller.show()

        assert model.accountCategoryInstance == accountCategory
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/accountCategory/list'

        populateValidParams(params)
        def accountCategory = new AccountCategory(params)

        assert accountCategory.save() != null

        params.id = accountCategory.id

        def model = controller.edit()

        assert model.accountCategoryInstance == accountCategory
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/accountCategory/list'

        response.reset()

        populateValidParams(params)
        def accountCategory = new AccountCategory(params)

        assert accountCategory.save() != null

        // test invalid parameters in update
        params.id = accountCategory.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/accountCategory/edit"
        assert model.accountCategoryInstance != null

        accountCategory.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/accountCategory/show/$accountCategory.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        accountCategory.clearErrors()

        populateValidParams(params)
        params.id = accountCategory.id
        params.version = -1
        controller.update()

        assert view == "/accountCategory/edit"
        assert model.accountCategoryInstance != null
        assert model.accountCategoryInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/accountCategory/list'

        response.reset()

        populateValidParams(params)
        def accountCategory = new AccountCategory(params)

        assert accountCategory.save() != null
        assert AccountCategory.count() == 1

        params.id = accountCategory.id

        controller.delete()

        assert AccountCategory.count() == 0
        assert AccountCategory.get(accountCategory.id) == null
        assert response.redirectedUrl == '/accountCategory/list'
    }
}
