package PassVault.security

import org.codehaus.groovy.grails.plugins.springsecurity.GormUserDetailsService
import org.codehaus.groovy.grails.plugins.springsecurity.GrailsUser
import org.codehaus.groovy.grails.plugins.springsecurity.GrailsUserDetailsService
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import PassVault.SecUser

class MyUserDetailsService extends GormUserDetailsService implements GrailsUserDetailsService{

    /**
     * Some Spring Security classes (e.g. RoleHierarchyVoter) expect at least
     * one role, so we give a user with no granted roles this one which gets
     * past that restriction but doesn't grant anything.
     */
    static final List NO_ROLES = [new GrantedAuthorityImpl(SpringSecurityUtils.NO_ROLE)]

    /*UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException {  //CAUSES LOGIN TO JUST HANG (commented out when implementing per user hash)
        return loadUserByUsername(username)
    }

    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {                      //CAUSES EXCEPTION "GrailsUser doesn't have property: "salt" (commented out when implementing per user hash)
        SecUser user = SecUser.findByUsername(username.toUpperCase())
        if (!user) throw new UsernameNotFoundException(
                'User not found', username.toUpperCase())
        def authorities

        SecUser.withTransaction() {
            def roles = user.getAuthorities()
            authorities = roles.collect {
                new GrantedAuthorityImpl(it.authority)
            }
        }

        return new GrailsUser(user.username, user.password, user.enabled, !user.accountExpired,
                !user.passwordExpired, !user.accountLocked, authorities ?: NO_ROLES, user.id)
    }*/

    protected UserDetails createUserDetails(SecUser user, Collection authorities)
    {
        user.twoFactorAuthSend()
        new MyUserDetails((GrailsUser) super.createUserDetails(user, authorities), user.salt)
    }
}