--------------------------------------------------------
--  File created - Saturday-November-23-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table SEC_ROLE
--------------------------------------------------------

  CREATE TABLE "SEC_ROLE" 
   (	"ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"AUTHORITY" VARCHAR2(255 CHAR)
   ) ;
REM INSERTING into SEC_ROLE
SET DEFINE OFF;
Insert into SEC_ROLE (ID,VERSION,AUTHORITY) values (1,0,'ROLE_USER');
Insert into SEC_ROLE (ID,VERSION,AUTHORITY) values (2,0,'ROLE_ADMIN');
--------------------------------------------------------
--  DDL for Index SEC_ROLEPK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SEC_ROLEPK" ON "SEC_ROLE" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTHORITY_UNIQ_1380757804728
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTHORITY_UNIQ_1380757804728" ON "SEC_ROLE" ("AUTHORITY") 
  ;
--------------------------------------------------------
--  Constraints for Table SEC_ROLE
--------------------------------------------------------

  ALTER TABLE "SEC_ROLE" ADD CONSTRAINT "SEC_ROLEPK" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SEC_ROLE" MODIFY ("AUTHORITY" NOT NULL ENABLE);
  ALTER TABLE "SEC_ROLE" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "SEC_ROLE" MODIFY ("ID" NOT NULL ENABLE);



--------------------------------------------------------------------------------------------------

--SecUsers

--------------------------------------------------------------------------------------------------


 CREATE TABLE "SEC_USER"
   (	"ID" NUMBER(19,0),
	"VERSION" NUMBER(19,0),
	"ACCOUNT_EXPIRED" NUMBER(1,0),
	"ACCOUNT_LOCKED" NUMBER(1,0),
	"ENABLED" NUMBER(1,0),
	"PASSWORD" VARCHAR2(255 CHAR),
	"PASSWORD_EXPIRED" NUMBER(1,0),
	"USERNAME" VARCHAR2(255 CHAR),
	"EMAIL" VARCHAR2(255 CHAR),
  "SALT" VARCHAR2(255 BYTE)
   ) ;
REM INSERTING into SEC_USER
SET DEFINE OFF;
Insert into SEC_USER (ID,VERSION,ACCOUNT_EXPIRED,ACCOUNT_LOCKED,ENABLED,PASSWORD,PASSWORD_EXPIRED,USERNAME,EMAIL) values (3,0,0,0,1,'998ed4d621742d0c2d85ed84173db569afa194d4597686cae947324aa58ab4bb',0,'admin','brandon@brandonwagner.info');
--------------------------------------------------------
--  DDL for Index SEC_USERPK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SEC_USERPK" ON "SEC_USER" ("ID");
  CREATE UNIQUE INDEX "USERNAME_UNIQ_1380757804735" ON "SEC_USER" ("USERNAME");
  CREATE UNIQUE INDEX "EMAIL_UNIQ_13984713120398123" ON "SEC_USER" ("EMAIL");
--------------------------------------------------------
--  Constraints for Table SEC_USER
--------------------------------------------------------

  ALTER TABLE "SEC_USER" ADD CONSTRAINT "SEC_USERPK" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SEC_USER" MODIFY ("USERNAME" NOT NULL ENABLE);
  ALTER TABLE "SEC_USER" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "SEC_USER" MODIFY ("PASSWORD_EXPIRED" NOT NULL ENABLE);
  ALTER TABLE "SEC_USER" MODIFY ("PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "SEC_USER" MODIFY ("ENABLED" NOT NULL ENABLE);
  ALTER TABLE "SEC_USER" MODIFY ("ACCOUNT_LOCKED" NOT NULL ENABLE);
  ALTER TABLE "SEC_USER" MODIFY ("ACCOUNT_EXPIRED" NOT NULL ENABLE);
  ALTER TABLE "SEC_USER" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "SEC_USER" MODIFY ("ID" NOT NULL ENABLE);



----------------------------------------------------------------

----------------SECUSERSECROLE -> JOIN TABLE

----------------------------------------------------------------

CREATE TABLE "SEC_USER_SEC_ROLE"
(	"SEC_ROLE_ID" NUMBER(19,0),
   "SEC_USER_ID" NUMBER(19,0)
) ;
REM INSERTING into SEC_USER_SEC_ROLE
SET DEFINE OFF;
Insert into SEC_USER_SEC_ROLE (SEC_ROLE_ID,SEC_USER_ID) values (2,3);
--------------------------------------------------------
--  DDL for Index SEC_USER_SEC_PK
--------------------------------------------------------

CREATE UNIQUE INDEX "SEC_USER_SEC_PK" ON "SEC_USER_SEC_ROLE" ("SEC_ROLE_ID", "SEC_USER_ID")
;
--------------------------------------------------------
--  Constraints for Table SEC_USER_SEC_ROLE
--------------------------------------------------------

ALTER TABLE "SEC_USER_SEC_ROLE" ADD CONSTRAINT "SEC_USER_SEC_PK" PRIMARY KEY ("SEC_ROLE_ID", "SEC_USER_ID") ENABLE;
ALTER TABLE "SEC_USER_SEC_ROLE" MODIFY ("SEC_USER_ID" NOT NULL ENABLE);
ALTER TABLE "SEC_USER_SEC_ROLE" MODIFY ("SEC_ROLE_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table SEC_USER_SEC_ROLE
--------------------------------------------------------

ALTER TABLE "SEC_USER_SEC_ROLE" ADD CONSTRAINT "FK6630E2A776BF350" FOREIGN KEY ("SEC_USER_ID")
REFERENCES "SEC_USER" ("ID") ENABLE;
ALTER TABLE "SEC_USER_SEC_ROLE" ADD CONSTRAINT "FK6630E2AD2412F70" FOREIGN KEY ("SEC_ROLE_ID")
REFERENCES "SEC_ROLE" ("ID") ENABLE;