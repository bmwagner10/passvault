databaseChangeLog = {

	changeSet(author: "brandon (generated)", id: "1386034333760-1") {
		createTable(tableName: "account") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "accountPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "act_type_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "byte_password", type: "raw(255)")

			column(name: "email", type: "varchar2(255 char)")

			column(name: "password", type: "varchar2(255 char)")

			column(name: "pin", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}

			column(name: "salt", type: "varchar2(64 char)")

			column(name: "sec_user_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "security", type: "number(10,0)") {
				constraints(nullable: "false")
			}

			column(name: "username", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-2") {
		createTable(tableName: "account_category") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "account_categPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "cat_name", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}

			column(name: "cat_security", type: "number(10,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-3") {
		createTable(tableName: "account_type") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "account_typePK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "act_security", type: "number(10,0)") {
				constraints(nullable: "false")
			}

			column(name: "category_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "serv_name", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}

			column(name: "url", type: "varchar2(255 char)")
		}
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-4") {
		createTable(tableName: "sec_role") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sec_rolePK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "authority", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-5") {
		createTable(tableName: "sec_user") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sec_userPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "account_expired", type: "number(1,0)") {
				constraints(nullable: "false")
			}

			column(name: "account_locked", type: "number(1,0)") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp")

			column(name: "email", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}

			column(name: "enabled", type: "number(1,0)") {
				constraints(nullable: "false")
			}

			column(name: "last_logged_in", type: "timestamp")

			column(name: "last_update", type: "timestamp")

			column(name: "login_attempts", type: "number(10,0)")

			column(name: "password", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}

			column(name: "password_expired", type: "number(1,0)") {
				constraints(nullable: "false")
			}

			column(name: "salt", type: "varchar2(64 char)")

			column(name: "two_factor_pin", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}

			column(name: "username", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-6") {
		createTable(tableName: "sec_user_sec_role") {
			column(name: "sec_role_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "sec_user_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-7") {
		addPrimaryKey(columnNames: "sec_role_id, sec_user_id", constraintName: "sec_user_sec_PK", tableName: "sec_user_sec_role")
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-13") {
		createIndex(indexName: "cat_name_uniq_1386034333658", tableName: "account_category", unique: "true") {
			column(name: "cat_name")
		}
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-14") {
		createIndex(indexName: "authority_uniq_1386034333673", tableName: "sec_role", unique: "true") {
			column(name: "authority")
		}
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-15") {
		createIndex(indexName: "email_uniq_1386034333676", tableName: "sec_user", unique: "true") {
			column(name: "email")
		}
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-16") {
		createIndex(indexName: "username_uniq_1386034333681", tableName: "sec_user", unique: "true") {
			column(name: "username")
		}
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-17") {
		createSequence(sequenceName: "hibernate_sequence")
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-8") {
		addForeignKeyConstraint(baseColumnNames: "act_type_id", baseTableName: "account", constraintName: "FKB9D38A2DC97C028D", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "account_type", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-9") {
		addForeignKeyConstraint(baseColumnNames: "sec_user_id", baseTableName: "account", constraintName: "FKB9D38A2D776BF350", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "sec_user", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-10") {
		addForeignKeyConstraint(baseColumnNames: "category_id", baseTableName: "account_type", constraintName: "FK410E120CE120AEBA", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "account_category", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-11") {
		addForeignKeyConstraint(baseColumnNames: "sec_role_id", baseTableName: "sec_user_sec_role", constraintName: "FK6630E2AD2412F70", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "sec_role", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1386034333760-12") {
		addForeignKeyConstraint(baseColumnNames: "sec_user_id", baseTableName: "sec_user_sec_role", constraintName: "FK6630E2A776BF350", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "sec_user", referencesUniqueColumn: "false")
	}
}
