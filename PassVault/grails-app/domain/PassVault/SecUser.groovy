package PassVault

import groovy.sql.Sql
import util.SaltGenerator

class SecUser {
	transient springSecurityService
    transient mailService

	String username
    String email
	String password
    String salt
    Date dateCreated
    Date lastLoggedIn
    Date lastUpdate
    Integer loginAttempts
    String twoFactorPin
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
    Boolean authTwoFactor

	static constraints = {
		username blank: false, unique: true
        email blank: false, unique: true, email: true
        salt nullable: true, maxSize: 64
        password blank: false
        lastLoggedIn nullable: true
        loginAttempts nullable: true
        lastUpdate nullable: true
        dateCreated nullable: true
        twoFactorPin nullable: true
        authTwoFactor nullable: true
	}

	static mapping = {
		//password column: '`password`'
	}

	Set<SecRole> getAuthorities() {
		SecUserSecRole.findAllBySecUser(this).collect { it.secRole } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
        this.salt = SaltGenerator.generateSalt()
        password = springSecurityService.encodePassword(password, salt)
    }

    public void twoFactorAuthSend(){
        this.authTwoFactor = false
        def wholeAuthPin = SaltGenerator.generateSalt()
        def authPin = ""
        for(int i=0; i<6; i++)
            authPin += wholeAuthPin.charAt(i)
        authPin = authPin.toLowerCase();
        this.twoFactorPin = authPin.encodeAsSHA1()

        mailService.sendMail{
            to this.email
            subject "PassVault: Second Factor Authentication PIN"
            body 'Your PIN is: ' + authPin + '\n\n' + 'If you did not try to login at ' + new Date() + ' please change your password to PassVault.'
        }
    }

    public void addTwoFactorRole(){
        authTwoFactor = true
        this.save(flush: true)
    }

}
