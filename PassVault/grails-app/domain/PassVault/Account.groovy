package PassVault

import util.Encryptor
import util.SaltGenerator


class Account
{
    transient springSecurityService

    AccountType actType
    SecUser secUser

    Integer security

    String username
    String email
    String password
    byte[] bytePassword

    String pin
    String salt


    static constraints = {
        actType nullable: false
        secUser nullable: false
        salt nullable: true, maxSize: 64
        bytePassword nullable: true
        security nullable: false, size: 1..10
        username blank: false, nullable: false
        email nullable: true, blank: true
        password nullable: true, blank: true
    }

    static mapping = {
        //actType lazy: false
        actType fetch: 'join'
    }

    def beforeInsert() {
        encodePassword()
        encodePin()
    }

    def beforeUpdate() {
        if (isDirty('pin')) {
            encodePin()
        }
        if(isDirty('password')){
            encodePassword()
        }
    }

    @Override
    public String toString()
    {
        return actType.toString();
    }

    @Override
    protected void encodePin()
    {
        this.salt = SaltGenerator.generateSalt()
        this.pin = springSecurityService.encodePassword(pin, salt)
    }

    @Override
    protected Boolean verifyPin(String pin)
    {
        if(springSecurityService.encodePassword(pin, this.salt).equals(this.pin))
            return true
        else
            return false
    }

    @Override
    protected void encodePassword()
    {
        Encryptor enc = new Encryptor()
        enc.Helper(pin)
        this.password = enc.encrypt(password)
        this.bytePassword = enc.encrypt(password)
    }

    @Override
    protected String decodePassword(String pin)
    {
        Encryptor dec = new Encryptor()
        dec.Helper(pin)
        return dec.decrypt(this.password, pin)
    }

}
