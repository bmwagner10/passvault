package PassVault

//Account Categories would include: Social Media, Shopping, Professional Networking, Email, etc.
class AccountCategory {

    Integer catSecurity
    String catName



    static constraints = {
        catSecurity nullable: false, size: 1..10
        catName nullable: false, unique: true
    }

    static mapping = {
    }

    @Override
    public String toString()
    {
        return catName
    }
}
