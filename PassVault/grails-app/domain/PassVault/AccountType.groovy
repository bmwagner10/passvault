package PassVault

//This class will hold types such as Facebook, Instagram, Pinterest, Gmail, LinkedIn, etc.
class AccountType
{
    Integer actSecurity
    String servName
    String url
    AccountCategory category


    static constraints = {
        actSecurity nullable: false, size: 1..10
        servName nullable: false, blank: false
        url nullable: true, blank: true
        category nullable: false
    }

    static mapping = {
        //category lazy: true
        category fetch: 'join'
    }

    @Override
    public String toString()
    {
        return servName
    }
}
