package PassVault

import org.springframework.dao.DataIntegrityViolationException

class AccountCategoryController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [accountCategoryInstanceList: AccountCategory.list(params), accountCategoryInstanceTotal: AccountCategory.count()]
    }

    def create() {
        [accountCategoryInstance: new AccountCategory(params)]
    }

    def save() {
        def accountCategoryInstance = new AccountCategory(params)
        if (!accountCategoryInstance.save(flush: true)) {
            render(view: "create", model: [accountCategoryInstance: accountCategoryInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'accountCategory.label', default: 'AccountCategory'), accountCategoryInstance.id])
        redirect(action: "show", id: accountCategoryInstance.id)
    }

    def show(Long id) {
        def accountCategoryInstance = AccountCategory.get(id)
        if (!accountCategoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'accountCategory.label', default: 'AccountCategory'), id])
            redirect(action: "list")
            return
        }

        [accountCategoryInstance: accountCategoryInstance]
    }

    def edit(Long id) {
        def accountCategoryInstance = AccountCategory.get(id)
        if (!accountCategoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'accountCategory.label', default: 'AccountCategory'), id])
            redirect(action: "list")
            return
        }

        [accountCategoryInstance: accountCategoryInstance]
    }

    def update(Long id, Long version) {
        def accountCategoryInstance = AccountCategory.get(id)
        if (!accountCategoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'accountCategory.label', default: 'AccountCategory'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (accountCategoryInstance.version > version) {
                accountCategoryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'accountCategory.label', default: 'AccountCategory')] as Object[],
                          "Another user has updated this AccountCategory while you were editing")
                render(view: "edit", model: [accountCategoryInstance: accountCategoryInstance])
                return
            }
        }

        accountCategoryInstance.properties = params

        if (!accountCategoryInstance.save(flush: true)) {
            render(view: "edit", model: [accountCategoryInstance: accountCategoryInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'accountCategory.label', default: 'AccountCategory'), accountCategoryInstance.id])
        redirect(action: "show", id: accountCategoryInstance.id)
    }

    def delete(Long id) {
        def accountCategoryInstance = AccountCategory.get(id)
        if (!accountCategoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'accountCategory.label', default: 'AccountCategory'), id])
            redirect(action: "list")
            return
        }

        try {
            accountCategoryInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'accountCategory.label', default: 'AccountCategory'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'accountCategory.label', default: 'AccountCategory'), id])
            redirect(action: "show", id: id)
        }
    }
}
