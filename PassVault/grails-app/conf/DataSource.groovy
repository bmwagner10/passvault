dataSource {
    driverClassName = 'oracle.jdbc.driver.OracleDriver'
    dialect = org.hibernate.dialect.Oracle10gDialect
    pooled = true
    username = "passvaultdev"
    password = "passvaultdev"
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
        	//username = "passvaultprod"
        	//password = "passvaultprod"
            //dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
            //url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
            //url = 'jdbc:oracle:thin:@127.0.0.1:1521:orcl'
	        url = 'jdbc:oracle:thin:@10.0.1.126:1521:xe'
	        //url = 'jdbc:oracle:thin:@rsgeno5d.c1wmdznggn6o.us-east-1.rds.amazonaws.com:1521:rsgeno5d'
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
        }
    }
    production {
        dataSource {
        username = "passvaultprod"
        password = "passvaultprod"
    	url = 'jdbc:oracle:thin:@rsgeno5d.c1wmdznggn6o.us-east-1.rds.amazonaws.com:1521:rsgeno5d'
           
        }
    }
}
