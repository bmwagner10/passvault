// Place your Spring DSL code here
beans = {
    customPropertyEditorRegistrar(util.CustomPropertyEditorRegistrar)
    userDetailsService(PassVault.security.MyUserDetailsService)
            {grailsApplication = ref('grailsApplication')}
    saltSource(util.UserSaltSource)
            {userPropertyToUse = application.config.grails.plugins.springsecurity.dao.reflectionSaltSourceProperty}

}
