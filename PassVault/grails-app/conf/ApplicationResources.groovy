modules = {
    application {
        resource url:'css/mainLayout.css'
        resource url:'css/bootstrap.min.css'
        resource url:'js/application.js'
        resource url:'js/jquery-1.10.2.min.js'
        resource url:'js/bootstrap.min.js'
        resource url:'js/checkPassword.js'
        resource url:'js/jquery.zclip.min.js'
    }
}