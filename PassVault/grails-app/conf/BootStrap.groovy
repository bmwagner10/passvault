import PassVault.*;

class BootStrap {
    def springSecurityService

    def init = { servletContext ->

        def userRole = SecRole.findByAuthority('ROLE_USER') ?: new SecRole(authority: 'ROLE_USER').save(failOnError: true)
        def adminRole = SecRole.findByAuthority('ROLE_ADMIN') ?: new SecRole(authority: 'ROLE_ADMIN').save(failOnError: true)
//
//        AccountCategory = AccountCategory.findByCatName("Banking") ?: new AccountCategory(catName: 'Banking', catSecurity: 10).save(failOnError: true)
//        AccountCategory = AccountCategory.findByCatName("Social Media") ?: new AccountCategory(catName: 'Social Media', catSecurity: 5).save(failOnError: true)
//        AccountCategory = AccountCategory.findByCatName("Professional Networking") ?: new AccountCategory(catName: 'Professional Networking', catSecurity: 7).save(failOnError: true)
//        AccountCategory = AccountCategory.findByCatName("Online Shopping") ?: new AccountCategory(catName: 'Online Shopping', catSecurity: 9).save(failOnError: true)
//
//        AccountType = AccountType.findByServName("Facebook") ?: new AccountType(servName: 'Facebook', url: 'http://facebook.com', actSecurity: 6, category: PassVault.AccountCategory.findByCatName("Social Media")).save(failOnError: true)

        def adminUser = SecUser.findByUsername('admin') ?: new SecUser(
                username: 'admin',
                lastLoggedIn: new Date(),
                lastUpdate: new Date(),
                loginAttempts: 0,
                email: 'brandon@brandonwagner.info',
                password: 'password',
                enabled: true).save(failOnError: true)

        if (!adminUser.authorities.contains(adminRole)) {
            SecUserSecRole.create adminUser, adminRole
        }

    }
    def destroy = {
    }
}
