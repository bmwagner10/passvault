<%@ page import="PassVault.AccountType" %>

<div class="fieldcontain ${hasErrors(bean: accountTypeInstance, field: 'servName', 'error')} required">
    <label for="servName">
        <g:message code="accountType.servName.label" default="Service Name" />
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="servName" required="" value="${accountTypeInstance?.servName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: accountTypeInstance, field: 'actSecurity', 'error')} required">
	<label for="actSecurity">
		<g:message code="accountType.actSecurity.label" default="Account Security" />
		<span class="required-indicator">*</span>
	</label>
	<g:field max="10" min="1" name="actSecurity" type="number" value="${accountTypeInstance.actSecurity}" required=""/>
</div>


<div class="fieldcontain ${hasErrors(bean: accountTypeInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="accountType.url.label" default="Service URL" />
		
	</label>
	<g:textField name="url" value="${accountTypeInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: accountTypeInstance, field: 'category', 'error')} required">
	<label for="category">
		<g:message code="accountType.category.label" default="Category" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="category" name="category.id" from="${PassVault.AccountCategory.list()}" optionKey="id" required="" value="${accountTypeInstance?.category?.id}" class="many-to-one"/>
</div>

