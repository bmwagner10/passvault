
<%@ page import="PassVault.AccountType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <meta name="sub" content="account"/>
        <g:set var="entityName" value="${message(code: 'accountType.label', default: 'AccountType')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		%{--<div class="nav" role="navigation">--}%
			%{--<ul>--}%
				%{--<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
				%{--<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
			%{--</ul>--}%
		%{--</div>--}%

        <div class="container">
            <h1>Account Types</h1>
            <hr />
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            <table class="table">
                <thead>
                <tr>
                    <g:sortableColumn property="id" title="ID" />
                    <g:sortableColumn property="servName" title="Service Name" />
                    <g:sortableColumn property="actSecurity" title="Account Security" />
                    <g:sortableColumn property="url" title="URL" />
                    <th>Category</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${accountTypeInstanceList}" status="i" var="accountTypeInstance">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                        <td><g:link action="show" id="${accountTypeInstance.id}">${fieldValue(bean: accountTypeInstance, field: "id")}</g:link></td>
                        <td><g:link action="show" id="${accountTypeInstance.id}">${fieldValue(bean: accountTypeInstance, field: "servName")}</g:link></td>
                        <td><g:link action="show" id="${accountTypeInstance.id}">${fieldValue(bean: accountTypeInstance, field: "actSecurity")}</g:link></td>
                        <td><a href="${accountTypeInstance?.url}">${fieldValue(bean: accountTypeInstance, field: "url")}</a></td>
                        <td><g:link controller="AccountCategory" action="show" id="${accountTypeInstance.categoryId}">${fieldValue(bean: accountTypeInstance, field: "category")}</g:link></td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
	</body>
</html>
