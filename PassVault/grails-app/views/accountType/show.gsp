
<%@ page import="PassVault.AccountType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <meta name="sub" content="account"/>
        <g:set var="entityName" value="${message(code: 'accountType.label', default: 'AccountType')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-accountType" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-accountType" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list accountType">
			
				<g:if test="${accountTypeInstance?.actSecurity}">
				<li class="fieldcontain">
					<span id="actSecurity-label" class="property-label"><g:message code="accountType.actSecurity.label" default="Act Security" /></span>
					
						<span class="property-value" aria-labelledby="actSecurity-label"><g:fieldValue bean="${accountTypeInstance}" field="actSecurity"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountTypeInstance?.servName}">
				<li class="fieldcontain">
					<span id="servName-label" class="property-label"><g:message code="accountType.servName.label" default="Serv Name" /></span>
					
						<span class="property-value" aria-labelledby="servName-label"><g:fieldValue bean="${accountTypeInstance}" field="servName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountTypeInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="accountType.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${accountTypeInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountTypeInstance?.category}">
				<li class="fieldcontain">
					<span id="category-label" class="property-label"><g:message code="accountType.category.label" default="Category" /></span>
					
						<span class="property-value" aria-labelledby="category-label"><g:link controller="accountCategory" action="show" id="${accountTypeInstance?.category?.id}">${accountTypeInstance?.category?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${accountTypeInstance?.id}" />
					<g:link class="edit" action="edit" id="${accountTypeInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
