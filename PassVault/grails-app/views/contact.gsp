<%@ page import="PassVault.SecUser" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
        <meta name="sub" content="contact"/>
		<title>PassVault | Contact</title>
	</head>
	<body>
	        <div class="space"></div>
            <div class="jumbotron">
                    <h1>PassVault | Contact <img height="50em" width="50em" src="${resource(dir:'images', file:'passvault.jpg')}" /></h1>
                    <p class="lead">Contact Brandon: <br />bmwagner10@gmail.com</p>
                    <p class="lead"><a href="${createLink(controller: 'Login', action: 'about')}">Learn More</a></p>
                    <sec:ifNotLoggedIn>
                        <a href="${createLink(controller: 'SecUser', action: 'create')}" class="btn btn-large btn-success">Sign up today</a>
                    </sec:ifNotLoggedIn>
            </div>
	</body>
</html>
