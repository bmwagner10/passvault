<%@ page import="PassVault.SecUser; PassVault.Account" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta charset="UTF-8">
		<title><g:layoutTitle default="PassVault"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<g:layoutHead/>
		<r:require module="application"/>
		<r:layoutResources />
	</head>
	<body>
	    <header>
            <div class="navbar navbar-inverse navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                      <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="brand" href="${createLink(uri: '/')}">PassVault <img height="15em" width="15em" src="${resource(dir:'images', file:'passvault_inverted.gif')}" /></a>
                      <div class="nav-collapse collapse">
                        <ul class="nav">
                          <li class="${(pageProperty(name:'meta.sub') =='home')?'active':''}"><a href="${createLink(uri: '/')}">Home</a></li>
                          <li class="${(pageProperty(name:'meta.sub') =='about')?'active':''}"><a href="${createLink(controller: 'Login', action: 'about')}">About</a></li>
                          <li class="${(pageProperty(name:'meta.sub') =='contact')?'active':''}"><a href="${createLink(controller: 'Login', action: 'contact')}">Contact</a></li>
                          %{--<li class="${(pageProperty(name:'meta.sub') =='securityNews')?'active':''}"><a href="${createLink(controller: 'Login', action: 'securityNews')}">Security News</a></li>--}%
                          <sec:ifLoggedIn>
                              <g:if test="${SecUser.get(sec.loggedInUserInfo(field: 'id').toLong()).authTwoFactor}">
                                  <li class="dropdown ${(pageProperty(name:'meta.sub') =='account')?'active':''}">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Accounts <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                      <li><a href="${createLink(controller: 'Account', action: 'create')}">New Account</a></li>
                                      <li><a href="${createLink(controller: 'AccountType', action: 'create')}">New Type</a></li>
                                      <li><a href="${createLink(controller: 'AccountCategory', action: 'create')}">New Category</a></li>
                                      <li class="divider"></li>
                                      <li class="nav-header">Your Accounts</li>
                                      <g:each in="${Account.findAllBySecUser(SecUser.findById(sec.loggedInUserInfo(field: 'id').toLong()))}" var="act">
                                          <li><a href="${createLink(controller: 'Account', action: 'show', id: "${act?.id}")}">${act}</a></li>
                                      </g:each>
                                      <li class="nav-header">View All</li>
                                        <li><a href="${createLink(controller: 'Account', action: 'list')}">All Accounts</a></li>
                                        <li><a href="${createLink(controller: 'AccountType', action: 'list')}">All Account Types</a></li>
                                        <li><a href="${createLink(controller: 'AccountCategory', action: 'list')}">All Account Categories</a></li>
                                        <sec:ifAnyGranted roles="ROLE_ADMIN">
                                            <li><a href="${createLink(controller: 'SecUser', action: 'list')}">All Users</a></li>
                                        </sec:ifAnyGranted>

                                    </ul>
                                  </li>
                               </g:if>
                          </sec:ifLoggedIn>
                        </ul>
                         <sec:ifNotLoggedIn>
                            <form action='/PassVault/j_spring_security_check' method='POST' autocomplete='off' class="navbar-form pull-right">
                              <input name='j_username' id='username' class="span2" type="text" placeholder="Username/Email">
                              <input name='j_password' id='password' class="span2" type="password" placeholder="Password">
                              <button type="submit" class="btn" onclick="${remoteFunction(controller: 'SecUser', action: 'sendTwoFactorEmail', params: ' ')}">Sign in</button>
                            </form>
                         </sec:ifNotLoggedIn>
                         <sec:ifLoggedIn>
                             <div style="padding-top: 10px;">
                                 <span style="color: white;"><sec:username/></span>
                             <span style="color:white; padding-left:5px; padding-right:5px;"> | </span>
                             <g:link controller="logout" style="padding-left:20px; padding-right:20px; color: white;">Logout</g:link>
                         </span>
                         </sec:ifLoggedIn>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </header>

        <div class="space"></div>
        <div class="container">
            <div class="container-narrow">
		        <g:layoutBody/>
		    </div>
		</div>
        <br /><br /><br /><br /><br /><br />
		<footer>
		    &copy; 2014 Brandon Wagner
		</footer>

		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>
