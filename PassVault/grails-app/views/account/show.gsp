
<%@ page import="PassVault.Account" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <meta name="sub" content="account"/>
        <g:set var="entityName" value="${message(code: 'account.label', default: 'Account')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
        <center>
        <div class="container">
            <div id="show-account" class="content scaffold-show" role="main">
                <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
                </g:if>
                <fieldset>
                    <legend><h1>${accountInstance.actType}</h1></legend>
                    <div>
                        <h3>Username: ${accountInstance.username}</h3>
                    </div>
                    <div>
                        <h3>Email: ${accountInstance.email}</h3>
                    </div>
                    <div id="getPassword">
                        <g:submitToRemote id="${accountInstance?.id}" value="Get Password" action="pinPrompt" update="getPassword" class="btn btn-large btn-success"/>
                    </div>
                </fieldset>
                <br /><br />
                <g:form>
                    <fieldset class="buttons">
                        <g:hiddenField name="id" value="${accountInstance?.id}" />
                        <g:link class="edit btn btn-large btn-info" action="edit" id="${accountInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                        <g:actionSubmit class="delete btn btn-large btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    </fieldset>
                </g:form>
            </div>
        </div>
        </center>
	</body>
</html>
