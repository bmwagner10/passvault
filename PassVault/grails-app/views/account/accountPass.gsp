
<%@ page import="PassVault.Account" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <meta name="sub" content="account"/>
    <g:set var="entityName" value="${message(code: 'account.label', default: 'Account Password')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
%{--<div class="nav" role="navigation">--}%
    %{--<ul>--}%
        %{--<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
        %{--<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
        %{--<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
    %{--</ul>--}%
%{--</div>--}%
<center>
<div id="show-account" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list account">

        <g:if test="${accountInstance?.username}">
            <li class="fieldcontain">
                <span id="username-label" class="property-label"><g:message code="account.username.label" default="Username" /></span>

                <span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${accountInstance}" field="username"/></span>

            </li>
        </g:if>

        <g:if test="${accountInstance?.email}">
            <li class="fieldcontain">
                <span id="email-label" class="property-label"><g:message code="account.email.label" default="Email" /></span>

                <span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${accountInstance}" field="email"/></span>

            </li>
        </g:if>

        <g:if test="${accountInstance?.password}">
            <li class="fieldcontain">
                <span id="password-label" class="property-label"><g:message code="account.password.label" default="Password" /></span>

                <span class="property-value" aria-labelledby="password-label"><g:fieldValue bean="${accountInstance}" field="password"/></span>

            </li>
        </g:if>

        <g:if test="${accountInstance?.actType}">
            <li class="fieldcontain">
                <span id="actType-label" class="property-label"><g:message code="account.actType.label" default="Act Type" /></span>

                <span class="property-value" aria-labelledby="actType-label"><g:link controller="accountType" action="show" id="${accountInstance?.actType?.id}">${accountInstance?.actType?.encodeAsHTML()}</g:link></span>
            </li>
        </g:if>

        <g:if test="${accountInstance?.security}">
            <li class="fieldcontain">
                <span id="security-label" class="property-label"><g:message code="account.security.label" default="Security" /></span>

                <span class="property-value" aria-labelledby="security-label"><g:fieldValue bean="${accountInstance}" field="security"/></span>

            </li>
        </g:if>

    </ol>
    <g:form>
        <fieldset class="buttons">
            <g:hiddenField name="id" value="${accountInstance?.id}" />
            <g:link class="edit" action="edit" id="${accountInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
        </fieldset>
    </g:form>
</div>
</center>
</body>
</html>
