<%@ page import="PassVault.AccountType; PassVault.Account" %>


<div class="fieldcontain ${hasErrors(bean: accountInstance, field: 'actType', 'error')} required">
	<label for="actType">
		<g:message code="account.actType.label" default="Act Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="actType" name="actType.id" from="${AccountType.findAll()}" optionKey="id" required="" value="${accountInstance?.actType?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: accountInstance, field: 'security', 'error')} required">
	<label for="security">
		<g:message code="account.security.label" default="Account Security" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="security" min="1" max="10" type="number" value="${accountInstance.security}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: accountInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="account.username.label" default="Account Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${accountInstance?.username}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: accountInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="account.email.label" default="Account Email" />
		
	</label>
	<g:textField name="email" value="${accountInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: accountInstance, field: 'password', 'error')} ">
	<label for="password">
		<g:message code="account.password.label" default="Account Password" />
		
	</label>
	<g:passwordField name="password" value="${accountInstance?.password}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: accountInstance, field: 'password', 'error')} ">
    <label for="confirmPassword">
        <span id="confirmMessage" class="confirmMessage">Confirm Password</span>
    </label>
    <g:passwordField onkeyup="checkPass(); return false;" name="confirmPassword" value=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: accountInstance, field: 'pin', 'error')} ">
    <label for="pin">
        <g:message code="account.pin.label" default="PassVault PIN" />
    </label>
    <g:passwordField name="pin" value="${accountInstance?.pin}"/>
</div>


