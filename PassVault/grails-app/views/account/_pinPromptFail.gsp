<%@ page import="PassVault.Account" %>
<span style="color: red">PIN Verification Failed, Try Again</span>
<g:form>
    <label>PIN: </label>
    <g:passwordField autofocus="" name="pinVerify" width="8px" maxlength="8" />
    <g:submitToRemote id="${accountInstance?.id}" class="btn btn-success" value="Verify" action="pinVerification" update="getPassword"/>
</g:form>