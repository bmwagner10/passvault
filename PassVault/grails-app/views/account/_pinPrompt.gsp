<%@ page import="PassVault.Account" %>

<script>
    function showPass(){
        var buttonElement = document.getElementById("passButton");
        var element = document.getElementById("pass");

        element.setAttribute("type", "text");
        buttonElement.innerHTML = "Hide Password";
        buttonElement.setAttribute("onclick", "hidePass();");
    }
    function hidePass(){
        var buttonElement = document.getElementById("passButton");
        var element = document.getElementById("pass");
        buttonElement.innerHTML = "Show Password";
        element.setAttribute("type", "password");
        buttonElement.setAttribute("onclick", "showPass(); this.select();");
    }
</script>

<g:form>
    <label>PIN: </label>
    <g:passwordField autofocus="" name="pinVerify" width="8px" maxlength="64" />
    <g:submitToRemote name="getPasswordButton" id="${accountInstance?.id}" class="btn btn-success" value="Verify" action="pinVerification" update="getPassword"/>
</g:form>