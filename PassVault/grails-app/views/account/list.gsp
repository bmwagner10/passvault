
<%@ page import="PassVault.SecUser; PassVault.Account" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <meta name="sub" content="account"/>
        <g:set var="entityName" value="${message(code: 'account.label', default: 'Account')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		%{--<div class="nav" role="navigation">--}%
			%{--<ul>--}%
				%{--<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
				%{--<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
			%{--</ul>--}%
		%{--</div>--}%
    <div class="container">
        <h1>Accounts</h1>
        <hr />
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>

        <table class="table">
            <thead>
            <tr>
                <g:sortableColumn property="actType" title="Account Type" />
                <g:sortableColumn property="security" title="Account Security" />
                <g:sortableColumn property="username" title="Username" />
                <g:sortableColumn property="email" title="Email" />
            </tr>
            </thead>
            <tbody>
            <g:each in="${Account.findAllBySecUser(SecUser.findById(sec.loggedInUserInfo(field: 'id').toLong()))}" status="i" var="accountInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                    <td><g:link action="show" id="${accountInstance.id}">${fieldValue(bean: accountInstance, field: "actType")}</g:link></td>
                    <td><g:link action="show" id="${accountInstance.id}">${fieldValue(bean: accountInstance, field: "security")}</g:link></td>
                    <td><g:link action="show" id="${accountInstance.id}">${fieldValue(bean: accountInstance, field: "username")}</g:link></td>
                    <td><g:link action="show" id="${accountInstance.id}">${fieldValue(bean: accountInstance, field: "email")}</g:link></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
	</body>
</html>
