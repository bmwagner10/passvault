<%@ page import="PassVault.SecUser" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
        <meta name="sub" content="home"/>
		<title>PassVault</title>
	</head>
	<body>
        <sec:ifNotLoggedIn>
	        <div class="space"></div>
            <div class="jumbotron">
                    <h1>PassVault <img height="50em" width="50em" src="${resource(dir:'images', file:'passvault.jpg')}" /></h1>
                    <p class="lead">The one stop security solution.</p>
                    <p class="lead"><a href="${createLink(controller: 'Login', action: 'about')}">Learn More</a></p>
                    <a href="${createLink(controller: 'SecUser', action: 'create')}" class="btn btn-large btn-success">Sign up today</a>
            </div>
        </sec:ifNotLoggedIn>
        <sec:ifLoggedIn>
            <g:if test="${!SecUser.get(sec.loggedInUserInfo(field: 'id').toLong()).authTwoFactor}">
                <br /><br /><br />
                <center>
                    <fieldset>
                        <legend>Two Factor Authentication</legend>
                        <g:form method="post" controller="login" action="authenticateSecond" >
                            <div class="fieldcontain required">
                                <g:textField name="twoFactorPIN" id="twoFactorPIN" required="" value="" placeholder="Two Factor Authentication PIN" maxlength="6" max="6" />
                            </div>
                            <g:actionSubmit class="save" controller="login" action="authenticateSecond" value="Authenticate" />
                        </g:form>
                     </fieldset>
                    <br /><br /><br /><br /><br /><br />
                </center>
            </g:if>
            <g:if test="${SecUser.get(sec.loggedInUserInfo(field: 'id').toLong()).authTwoFactor}">
                <div class="jumbotron">
                    <h1>PassVault <img height="50em" width="50em" src="${resource(dir:'images', file:'passvault.jpg')}" /></h1>
                    <p class="lead">The one stop security solution.</p>
                </div>
            <center>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Quick Links</h3>
                    </div>
                    <div class="panel-body">
                      <g:each in="${PassVault.AccountType.all}" var="serviceLink">
                          <p class="text-center text-info"><a href="${serviceLink.url}">${serviceLink.servName}</a></p>
                      </g:each>
                    </div>
                </div>
            </center>
            </g:if>
        </sec:ifLoggedIn>
	</body>
</html>
