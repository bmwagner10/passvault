
<%@ page import="PassVault.AccountCategory" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <meta name="sub" content="account"/>
        <g:set var="entityName" value="${message(code: 'accountCategory.label', default: 'AccountCategory')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-accountCategory" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-accountCategory" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list accountCategory">
			
				<g:if test="${accountCategoryInstance?.catSecurity}">
				<li class="fieldcontain">
					<span id="catSecurity-label" class="property-label"><g:message code="accountCategory.catSecurity.label" default="Cat Security" /></span>
					
						<span class="property-value" aria-labelledby="catSecurity-label"><g:fieldValue bean="${accountCategoryInstance}" field="catSecurity"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountCategoryInstance?.catName}">
				<li class="fieldcontain">
					<span id="catName-label" class="property-label"><g:message code="accountCategory.catName.label" default="Cat Name" /></span>
					
						<span class="property-value" aria-labelledby="catName-label"><g:fieldValue bean="${accountCategoryInstance}" field="catName"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${accountCategoryInstance?.id}" />
					<g:link class="edit" action="edit" id="${accountCategoryInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
