
<%@ page import="PassVault.AccountCategory" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <meta name="sub" content="account"/>
        <g:set var="entityName" value="${message(code: 'accountCategory.label', default: 'AccountCategory')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		%{--<div class="nav" role="navigation">--}%
			%{--<ul>--}%
				%{--<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
				%{--<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
			%{--</ul>--}%
		%{--</div>--}%

        <div class="container">
            <h1>User Listing</h1>
            <hr />
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            <table class="table">
                <thead>
                <tr>
                    <g:sortableColumn property="catSecurity" title="ID" />
                    <g:sortableColumn property="catName" title="Category Name" />
                    <g:sortableColumn property="catSecurity" title="Category Security" />
                </tr>
                </thead>
                <tbody>
                <g:each in="${accountCategoryInstanceList}" status="i" var="accountCategoryInstance">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                        <td><g:link action="show" id="${accountCategoryInstance.id}">${fieldValue(bean: accountCategoryInstance, field: "id")}</g:link></td>
                        <td>${fieldValue(bean: accountCategoryInstance, field: "catName")}</td>
                        <td><g:link action="show" id="${accountCategoryInstance.id}">${fieldValue(bean: accountCategoryInstance, field: "catSecurity")}</g:link></td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
	</body>
</html>
