<%@ page import="PassVault.AccountCategory" %>


<div class="fieldcontain ${hasErrors(bean: accountCategoryInstance, field: 'catName', 'error')} ">
	<label for="catName">
		<g:message code="accountCategory.catName.label" default="Category Name" />
		
	</label>
	<g:textField name="catName" value="${accountCategoryInstance?.catName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: accountCategoryInstance, field: 'catSecurity', 'error')} required">
    <label for="catSecurity">
        <g:message code="accountCategory.catSecurity.label" default="Category Security" />
        <span class="required-indicator">*</span>
    </label>
    <g:field max="10" min="1" name="catSecurity" type="number" value="${accountCategoryInstance.catSecurity}" required=""/>
</div>

