
<%@ page import="PassVault.SecUser" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'secUser.label', default: 'SecUser')}" />
		<title>PassVault | User Details</title>
	</head>
	<body>
		%{--<div class="nav" role="navigation">--}%
			%{--<ul>--}%
				%{--<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
				%{--<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
				%{--<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
			%{--</ul>--}%
		%{--</div>--}%
		%{--<div id="show-secUser" class="content scaffold-show" role="main">--}%
    <center>
        <div class="container">
			<h1>User Details</h1>
            <hr />
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
				<g:if test="${secUserInstance?.username}">
					<h2>Username: </h2>
                    <h3 style="color: darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<g:fieldValue bean="${secUserInstance}" field="username"/></h3>
				</g:if>

                <g:if test="${secUserInstance?.email}">
                    <h2>Email: </h2>
                    <h3 style="color: darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<g:fieldValue bean="${secUserInstance}" field="email"/></h3>
                </g:if>

                <g:if test="${secUserInstance?.dateCreated}">
                    <h2>Date Created: </h2>
                    <h3 style="color: darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<g:fieldValue bean="${secUserInstance}" field="dateCreated"/></h3>
                </g:if>

                <g:if test="${secUserInstance?.lastLoggedIn}">
                    <h2>Last Login: </h2>
                    <h3 style="color: darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<g:fieldValue bean="${secUserInstance}" field="lastLoggedIn"/></h3>
                </g:if>
			
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${secUserInstance?.id}" />
					<g:link class="edit" action="edit" id="${secUserInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
    </center>
	</body>
</html>
