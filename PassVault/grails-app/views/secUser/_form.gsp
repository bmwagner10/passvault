<%@ page import="PassVault.SecRole; PassVault.SecUser" %>



<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="secUser.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${secUserInstance?.username}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'email', 'error')} required">
    <label for="email">
        <g:message code="secUser.email.label" default="Email" />
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="email" required="" value="${secUserInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="secUser.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:passwordField name="password" required="" value=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'password', 'error')} required">
    <label for="confirmPassword">
        <g:message code="secUser.Confirmpassword.label" default="Confirm Password" />
        <span class="required-indicator">*</span>
    </label>
    <g:passwordField name="confirmPassword" required="" value=""/>
</div>



