<%@ page import="PassVault.SecUser" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
        <meta name="sub" content="about"/>
		<title>PassVault | About</title>
	</head>
	<body>
	        <div class="space"></div>
            <div class="jumbotron">
                    <h1>PassVault | About <img height="50em" width="50em" src="${resource(dir:'images', file:'passvault.jpg')}" /></h1>
                    <p class="lead">The one stop security solution.</p>
                    <p>PassVault provides an online password management solution for any sized organization and individuals.
                    Password management has become a necessity in todays world of security risks and the storage of high risk data items
                    on the web. We need stronger passwords and unique passwords for each of our accounts. PassVault allows you to enter in your
                     account information and reference our site to login to all your other accounts. Some people might be hessitant to trust
                    one system to handle their passwords for all of their accounts. PassVault hopes to mitigate that risk by providing a wide
                     variety of security implemented on our site. We use two step authentication to assure you are who you say you are when logging in.
                    We also use a short pin to encrypt each of your accounts' passwords. This gives you the ability to have one complex global password
                     for PassVault and remember a variable digit pin for each of your accounts (or they could be the same depending on how secure you want to be).
                     </p>
                    <p class="lead">What About a Break In? </p>
                    <p>We store your account PINs, two-factor PINs, and PassVault password in a hashed format. We use the secure SHA-256 hash algorithm.
                     All account passwords are encrypted with your cleartext account PIN. We store the hashed copy of the PIN which means that we can't even
                     access your password (So don't forget your PIN!).</p>
                    <p class="lead">Is it really that secure?</p>
                    <p>PassVault tries to balance usability with security in managing your credentials. This will obviously have some security implications.
                 However, we feel that this system is much better than traditional methods of writing passwords down, using a simple password, using the same password for all account.
                 Our efforts are to create a simple, intuitive system to manage passwords.</p>
                    <p class="lead">What does the future hold?</p>
                    <p>PassVault is not a finished product. I plan on developing the system to provide more system feedback for users. The system will alert the user
                     to change their password based on how secure the account is (maybe every year, every month, every 5 years, etc.). I would also like the system to
                     cater more towards corporations. PassVault will be able to handle user sharing of credentials in a secure manner (as secure as sharing can be at least). This is
                     often very helpful in a corporate environment where a department might need to have passwords to a system available to all the staff.</p>
                <sec:ifNotLoggedIn>
                    <a href="${createLink(controller: 'SecUser', action: 'create')}" class="btn btn-large btn-success">Sign up today</a>
                </sec:ifNotLoggedIn>
            </div>

	</body>
</html>
